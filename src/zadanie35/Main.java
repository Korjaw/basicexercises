package zadanie35;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String liniaLiczba1;
        String liniaLiczba2;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj pierwsza liczbe");
        liniaLiczba1 = wejscie.nextLine();
        System.out.println("Podales pierwsza liczbe rowna: " + liniaLiczba1);
        System.out.println("Podaj druga liczbe");
        liniaLiczba2 = wejscie.nextLine();
        System.out.println("Podales druga liczbe rowna: " + liniaLiczba2);

        float liczba1 = Float.parseFloat(liniaLiczba1);
        float liczba2 = Float.parseFloat(liniaLiczba2);

       /* if (liczba2 < 0) {
            System.out.println("Druga liczba jest ujemna");
        } else {
            float wynik = liczba1 / liczba2;
            System.out.println("Wynik: " + wynik);
        }*/

        try {
            if (liczba2 < 0) throw new IllegalArgumentException();
            float wynik = liczba1 / liczba2;
            System.out.println("Wynik: " + wynik);

        } catch (IllegalArgumentException e) {
            System.out.println(e + "Druga liczba jest ujemna");
        }
    }
}
