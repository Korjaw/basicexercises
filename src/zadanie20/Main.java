package zadanie20;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        String liniaLiczba;
        boolean nietrafione = true;

        Random generator = new Random();
        int number = generator.nextInt(99) + 1;

        while (nietrafione) {
            Scanner wyjscie = new Scanner(System.in);
            System.out.print("Podaj liczbe: ");
            liniaLiczba = wyjscie.nextLine();
            int liczba = Integer.parseInt(liniaLiczba);
            if (liczba == number) {
                System.out.println("Gratulacje");
                nietrafione=false;
            } else if (liczba > number) {
                System.out.println("Podales za duza wartosc");
            } else {
                System.out.println("Podales za mala wartosc");
            }
        }
    }
}
