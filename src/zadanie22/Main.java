package zadanie22;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String liniaLiczba;

        Scanner wyjscie = new Scanner(System.in);
        System.out.println("Dla podanej liczby calkowitej wyswietla jej dzielniki");
        liniaLiczba = wyjscie.nextLine();

        int liczba = Integer.parseInt(liniaLiczba);

        for (int i = 1; i<=liczba; i++) {
            if (liczba%i == 0) {
                System.out.print(i + " ");
            }
        }

    }
}
