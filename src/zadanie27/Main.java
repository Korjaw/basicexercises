package zadanie27;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String liniaLiczba;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj liczbe N, gdzie otrzymasz tabliczke mnozenia tej liczby do NxN: ");
        liniaLiczba = wejscie.nextLine();
        int liczba = Integer.parseInt(liniaLiczba);
        int wynik = 0;

        System.out.println("Podana liczba to: " + liczba);

        for (int i = 0; i <= liczba; i++) {
            for (int j = 0; j <= liczba; j++) {
                wynik = i * j;
                System.out.println(i + "x" + j + " = " + wynik);
            }
        }

    }
}
