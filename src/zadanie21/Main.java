package zadanie21;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String tree = "";
        String liniaHeight;

        Scanner wyjscie = new Scanner(System.in);
        liniaHeight = wyjscie.nextLine();
        int height = Integer.parseInt(liniaHeight);

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height - i; j++) {
                if (j==0) {
                    tree+="";
                } else{
                    tree += " ";
                }
            }
            for (int k = 0; k < 2*i+1; k++) {
                tree+="*";
            }
            tree+="\n";
        }
        System.out.println(tree);
    }
}
