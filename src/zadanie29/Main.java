package zadanie29;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String liniaCiagZnakow;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj ciag znakow");
        liniaCiagZnakow = wejscie.nextLine();

        System.out.println(liniaCiagZnakow);

        int counter = 0;
        char LastChar=liniaCiagZnakow.charAt(liniaCiagZnakow.length()-1);
        for (int i=0; i<=liniaCiagZnakow.length()-1; i++) {
            if (liniaCiagZnakow.charAt(i)==LastChar) {
                counter++;
            }
        }
        System.out.println("Ostatni znak to: " + (liniaCiagZnakow.charAt(liniaCiagZnakow.length()-1)) + " i wystepuje: " + counter + " razy");

    }
}
