package zadanie24;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] tab = new int[10];
        Random generator = new Random();
        System.out.print("Zawartosc tablicy: ");
        for(int i=0;i<10;i++)
        {
            tab[i]=generator.nextInt(21) -10;
            System.out.print(tab[i] + " ");
            int max = tab[0];
            int min = tab[0];
        }

        double max = tab[0];
        double min = tab[0];
        double suma = 0;

        for (int l : tab) {
            if (l > max) {
                max = l;
            } else if (l < min) {
                min = l;
            }
            suma+=l;
        }
        double wynik = max + min;
        System.out.println("");
        System.out.println("Min: " + min + " Max: " + max);

        double srednia = suma/tab.length;
        System.out.println("Srednia:  " + srednia + " , a suma: " + suma);

        System.out.print("Mniejszych od śr.: ");
        int licznikMniej = 0;
        int licznikWiecej = 0;
        for (int i = 9; i >= 0; i--) {
            if (tab[i]<srednia) {
                licznikMniej++;
            }
            if (tab[i]>srednia) {
                licznikWiecej++;
            }
        }
        System.out.print(licznikMniej);
        System.out.println("");
        System.out.println("Wiekszych od sr.: " + licznikWiecej);

        System.out.println("Liczby w odwrotnej kolejności: ");
        for (int i = 9; i >= 0; i--) {
            System.out.print(tab[i] + " ");
        }



    }
}

