package zadanie30;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String liniaCiagZnakow;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj ciag znakow");
        liniaCiagZnakow = wejscie.nextLine();

        char[] ciagZnakow = liniaCiagZnakow.toCharArray();

        for (int i=ciagZnakow.length-1; i>=0; i--) {
            System.out.print(ciagZnakow[i]);
        }

    }
}
