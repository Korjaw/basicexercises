package zadanie34;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String liniaTekstDoSzyfrowania;
        String liniaLiczbaN;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj ciag znakow");
        liniaTekstDoSzyfrowania = wejscie.nextLine();
        System.out.println("Podales: " + liniaTekstDoSzyfrowania);

        liniaLiczbaN = wejscie.nextLine();
        int liczbaN = Integer.parseInt(liniaLiczbaN);

        for (int i = 0; i < liniaTekstDoSzyfrowania.length(); i++) {
            if (liniaTekstDoSzyfrowania.charAt(i) + liczbaN >= 122) {
                int wynik = liniaTekstDoSzyfrowania.charAt(i)+liczbaN-122;
                char poczatek = 'a';
                System.out.print((char) (poczatek + wynik -1) + "");
            } else if (liniaTekstDoSzyfrowania.charAt(i) + liczbaN <= 97) {
                int wynik = 97-liczbaN-liniaTekstDoSzyfrowania.charAt(i);
                //System.out.println(wynik);
                char koniec = 'z';
                System.out.print((char)(koniec - wynik+1));
            } else {
                System.out.print((char) (liniaTekstDoSzyfrowania.charAt(i) + liczbaN) + "");
            }
        }
    }
}
