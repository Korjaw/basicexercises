package zadanie11;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String liczba_pierwsza;
        String znak_operacji;
        String liczba_druga;

        Scanner wejscie = new Scanner(System.in);

        System.out.println("To jest kalkulator, bedziesz musial podac pierwsze liczbe, nastepnie znak operacji i na koniec druga liczbe. Najpierw podaj pierwsza liczbe: ");
        liczba_pierwsza = wejscie.nextLine();
        System.out.println("Pierwsza liczba to: " + liczba_pierwsza);

        System.out.println("Podaj symbol operacji arytmetycznej: ");
        znak_operacji = wejscie.nextLine();
        System.out.println("Znak operacji to: " + znak_operacji);

        System.out.println("Podaj druga liczbe");
        liczba_druga = wejscie.nextLine();
        System.out.println("Druga liczba to: " + liczba_druga);

        if (znak_operacji.equals("+")) {
            int liczba_1 = Integer.parseInt(liczba_pierwsza);
            int liczba_2 = Integer.parseInt(liczba_druga);
            int wynik = liczba_1 + liczba_2;
            System.out.println("Wynik operacji to: " + liczba_1 + " " + znak_operacji + " " + liczba_2 + " = " +  wynik);
        } else if (znak_operacji.equals("-")) {
            int liczba_1 = Integer.parseInt(liczba_pierwsza);
            int liczba_2 = Integer.parseInt(liczba_druga);
            int wynik = liczba_1 - liczba_2;
            System.out.println("Wynik operacji to: " + liczba_1 + " " + znak_operacji + " " + liczba_2 + " = " +  wynik);
        } else if (znak_operacji.equals("*")) {
            int liczba_1 = Integer.parseInt(liczba_pierwsza);
            int liczba_2 = Integer.parseInt(liczba_druga);
            int wynik = liczba_1 * liczba_2;
            System.out.println("Wynik operacji to: " + liczba_1 + " " + znak_operacji + " " + liczba_2 + " = " +  wynik);
        } else if (znak_operacji.equals("/")) {
            int liczba_1 = Integer.parseInt(liczba_pierwsza);
            if (liczba_druga.equals("0")) {
                System.err.println("Nie dzielimy przez 0");
            }
            int liczba_2 = Integer.parseInt(liczba_druga);
            int wynik = liczba_1 / liczba_2;
            System.out.println("Wynik operacji to: " + liczba_1 + " " + znak_operacji + " " + liczba_2 + " = " +  wynik);
        } else {
            System.err.println("Bledny znak operacji");
        }


    }
}
