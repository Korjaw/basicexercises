package zadanie36;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String liniaA;
        String liniaB;
        String liniaC;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj wspolczynnik A: ");
        liniaA = wejscie.nextLine();
        System.out.println("Podaj wspolczynnik B: ");
        liniaB = wejscie.nextLine();
        System.out.println("Podaj wspolczynnik C: ");
        liniaC = wejscie.nextLine();

        int a = Integer.parseInt(liniaA);
        int b = Integer.parseInt(liniaB);
        int c = Integer.parseInt(liniaC);

        System.out.println("A: " + liniaA + " B: " + liniaB + " C: " + liniaC);
        System.out.println("Rownanie ma postac: " + a + "x2 + " + b + "x + " + c + " = 0");

        if (a==0) {
            System.out.println("Rownanie nie jest kwadratowe");
        } else {
            double delta=b*b-4*a*c;
            if (delta<0) {
                System.out.println("Delta < 0 \n -> rownanie nie ma rozwiazan");
            } else if (delta==0) {
                double wynik=-b/2*a;
                System.out.println("Rozwiazanie: delta= " + delta + " x0=" + wynik);
            } else if (delta>0) {
                double wynik;
                double wynik2;
                wynik = (-b-Math.sqrt(delta))/2*a;
                System.out.println("delta= " + delta + " x1= " + wynik);
                wynik2 = (-b+Math.sqrt(delta))/2*a;
                System.out.println("delta= " + delta + " x2= " + wynik2);
            }
        }



    }
}
