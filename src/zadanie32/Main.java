package zadanie32;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String liniaCiagZnakow;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj ciag znakow");
        liniaCiagZnakow = wejscie.nextLine();
        System.out.println("Podales: " + liniaCiagZnakow);

        int sum = 0;

        for (int i=0; i<liniaCiagZnakow.length(); i++) {
            if (Character.isDigit(liniaCiagZnakow.charAt(i))) {
                sum = sum + Integer.parseInt(liniaCiagZnakow.charAt(i) + "");
            }
        }
        System.out.println(sum);
    }
}
