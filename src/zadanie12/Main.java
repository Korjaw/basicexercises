package zadanie12;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String liczba;

        Scanner wejscie = new Scanner(System.in);

        System.out.println("Podaj liczbe: ");
        liczba = wejscie.nextLine();
        int liczba1 = Integer.parseInt(liczba);

        System.out.print("a: ");
        for (int i = 0; i <= liczba1; i++) {
            if (i % 2 == 0) {
                continue;
            }
            if (i == liczba1) {
                System.out.println(i);
                break;
            }
            System.out.print(i + ", ");
        }

        System.out.print("b: ");
        for (int i = 3; i <= 100; i++) {
            if (i == 100) {
                System.out.println(i);
                break;
            }
            if (i % 3 == 0 | i % 5 == 0) {
                System.out.print(i + ", ");
            }
        }

        String gorny_przedzial;
        String dolny_przedzial;

        Scanner wejscie_przedzial = new Scanner(System.in);

        System.out.println("Podaj dolny przedzial: ");
        dolny_przedzial = wejscie_przedzial.nextLine();
        int dolny_przedzial1 = Integer.parseInt(dolny_przedzial);

        System.out.println("Podaj gorny przedzial: ");
        gorny_przedzial = wejscie_przedzial.nextLine();
        int gorny_przedzial1 = Integer.parseInt(gorny_przedzial);

        System.out.print("Przedzial od " + dolny_przedzial1 + " do " + gorny_przedzial + " : ");
        for (int i=dolny_przedzial1; i<gorny_przedzial1; i++) {
            if (i%6 == 0) {
                System.out.print(i + " ");
            }
        }


    }



}
