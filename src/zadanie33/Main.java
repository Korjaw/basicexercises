package zadanie33;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String liniaCiagZnakow;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj ciag znakow");
        liniaCiagZnakow = wejscie.nextLine();
        System.out.println("Podales: " + liniaCiagZnakow);

        int licznik = 0;
        int licznik2 = 0;
        int flag = 0;

        for (int i = 0; i < liniaCiagZnakow.length(); i++) {

            if (liniaCiagZnakow.charAt(i) == '(') {
                licznik++;
            }
            if (liniaCiagZnakow.charAt(i) == ')') {
                licznik2++;
            }
            if (licznik2 > licznik) {
                System.err.println("Bledne sparowanie nawiasow");
                flag = 1;
                break;
            }

        }
        if (flag == 0) {
            System.out.println("OK");
        }

        System.out.println("Liczba '(' : " + licznik + " ')' : " + licznik2);
    }
}
