package zadanie18;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner wyjscie = new Scanner(System.in);

        System.out.println("Podaj ile liczb chcesz podac");

        String liniaLiczba;
        liniaLiczba = wyjscie.nextLine();

        int dlTabeli = Integer.parseInt(liniaLiczba);

        int[] liczby = new int[dlTabeli];

        for (int i = 0; i < liczby.length; i++) {
            System.out.println("Podaj " + (i + 1) + " liczbe z " + liniaLiczba);
            String liczba = wyjscie.nextLine();
            liczby[i] = Integer.parseInt(liczba);
        }

        System.out.print("Wczytane liczby to: ");
        for (int l : liczby) {
            System.out.print(l + " ");
        }

        System.out.println("");

        double max = liczby[0];
        double min = liczby[0];
        double suma = 0;

        for (int l : liczby) {
            if (l > max) {
                max = l;
            } else if (l < min) {
                min = l;
            }
            suma+=l;
        }
         double wynik = max + min;
        System.out.println("Suma najwiekszej (" + max + ") i najmniejszej (" + min + ") = " + wynik);

        double srednia = suma/dlTabeli;
        System.out.println("Srednia arytmetyczna wynosi: " + srednia);

    }
}



