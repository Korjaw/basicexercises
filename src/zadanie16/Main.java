package zadanie16;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String liniaLiczba;
        Scanner wejscie = new Scanner(System.in);
        boolean koniec = false;

        do {
            liniaLiczba = wejscie.nextLine();
            int liczba = Integer.parseInt(liniaLiczba);
            if (liczba <= 0)
                koniec = true;
            else {
                System.out.println("Hello World");
            }
        }
        while (!koniec);

        System.out.println("Koniec programu");

    }
}
