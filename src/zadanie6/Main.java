package zadanie6;

public class Main {

    public static void main(String[] args) {
        float ocena_matematyka = 3;
        float ocena_chemia = 5;
        float ocena_j_polski = 1;
        float ocena_j_angielski = 1;
        float ocena_wos = 1;
        float ocena_informatyka = 6;
        String matematyka = "matematyka";
        String chemia = "chemia";
        String polski = "j.polski";
        String angielski = "j.angielski";
        String wos = "wos";
        String informatyka = "informatyka";

        float liczba_przedmiotow = 6;
        float liczba_scisle = 3;
        float liczba_humanistyczne = liczba_przedmiotow-liczba_scisle;

        float wynik_all = (ocena_matematyka+ocena_chemia+ocena_j_polski+ocena_j_angielski+ocena_wos+ocena_informatyka)/liczba_przedmiotow;
        float wynik_scisle = (ocena_matematyka+ocena_chemia+ocena_informatyka)/liczba_scisle;
        float wynik_pozostale = (ocena_j_angielski+ocena_j_polski+ocena_wos)/liczba_humanistyczne;

        System.out.print("Srednia (all): ");
        System.out.format("%-10.2f%n", wynik_all);
        System.out.print("Srednia (scisle) : ");
        System.out.format("%-10.2f%n", wynik_scisle);
        System.out.print("Srednia (pozostale) : ");
        System.out.format("%-10.2f%n", wynik_pozostale);

        if (wynik_all == 1) {
            System.out.println("Srednia z wszystkich przedmiotów jest niedostateczna");
        }
        if (wynik_scisle == 1) {
            System.out.println("Srednia z przedmiotow scislych jest niedostateczna");
        }
        if (wynik_pozostale == 1) {
            System.out.println("Srednia z przedmiotow hum. jest niedostateczna");
        }

        if (ocena_matematyka == 1) {
            System.out.println("Ocena z " + matematyka + " jest niedostateczna");
        }
        if (ocena_informatyka == 1) {
            System.out.println("Ocena z " + informatyka + " jest niedostateczna");
        }
        if (ocena_j_angielski == 1) {
            System.out.println("Ocena z " + angielski + " jest niedostateczna");
        }
        if (ocena_j_polski == 1) {
            System.out.println("Ocena z " + polski + " jest niedostateczna");
        }
        if (ocena_chemia == 1) {
            System.out.println("Ocena z " + chemia+ " jest niedostateczna");
        }
        if (ocena_wos == 1) {
            System.out.println("Ocena z " + wos + " jest niedostateczna");
        }


    }
}
