package zadanie9;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String waga;
        String wzrost;
        String wiek;

        Scanner wejscie = new Scanner(System.in);

        waga = wejscie.nextLine();
        System.out.println("Teraz podaj wage: " + waga);

        int kg = Integer.parseInt(waga);

        wzrost = wejscie.nextLine();
        System.out.println("Teraz podaj wzrost: " + wzrost);

        int cm = Integer.parseInt(wzrost);

        wiek = wejscie.nextLine();
        System.out.println("Teraz podaj wiek: " + wiek);

        int lat = Integer.parseInt(wiek);

        if (cm < 150 | cm > 220) {
            System.out.println("ERROR ! Musisz miec wiecej niz 150 cm i mniej niz 220 cm wzrostu, masz: " + cm + " cm");
        } else if (kg > 180) {
            System.out.println("ERROR ! Musisz wazyc mniej niz 180kg, wazysz: " + kg);
        } else if (lat < 10 | lat > 80) {
            System.out.println("ERROR ! Nie mozesz wejsc na kolejke, gdyz masz: " + lat);
        } else {
            System.out.println("Mozesz wejsc na kolejke");
        }

    }
}
