package zadanie25;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int licznik1 = 0;
        int licznik2 = 0;
        int licznik3 = 0;
        int licznik4 = 0;
        int licznik5 = 0;
        int licznik6 = 0;
        int licznik7 = 0;
        int licznik8 = 0;
        int licznik9 = 0;
        int licznik10 = 0;

        int[] tab = new int[20];
        Random generator = new Random();
        System.out.print("Wylosowane liczby: ");

        for (int i = 0; i < 20; i++) {
            tab[i] = generator.nextInt(10) + 1;
            System.out.print(tab[i] + " ");
            if (1 == tab[i]) {
                licznik1++;
            } else if (2 == tab[i]) {
                licznik2++;
            } else if (3 == tab[i]) {
                licznik3++;
            } else if (4 == tab[i]) {
                licznik4++;
            } else if (5 == tab[i]) {
                licznik5++;
            } else if (6 == tab[i]) {
                licznik6++;
            } else if (7 == tab[i]) {
                licznik7++;
            } else if (8 == tab[i]) {
                licznik8++;
            } else if (9 == tab[i]) {
                licznik9++;
            } else if (10 == tab[i]) {
                licznik10++;
            }
        }
        System.out.println("");
        System.out.println("Wystapienia: ");
        System.out.println("1 - " + licznik1);
        System.out.println("2 - " + licznik2);
        System.out.println("3 - " + licznik3);
        System.out.println("4 - " + licznik4);
        System.out.println("5 - " + licznik5);
        System.out.println("6 - " + licznik6);
        System.out.println("7 - " + licznik7);
        System.out.println("8 - " + licznik8);
        System.out.println("9 - " + licznik9);
        System.out.println("10 - " + licznik10);


    }
}
